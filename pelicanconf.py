AUTHOR = 'Tanad Aaron'
SITENAME = 'tanadaaron.net'
SITEURL = 'colmoneill.gitlab.io/tanadaaron.net'

PATH = 'content'
OUTPUT_PATH = 'public'
STATIC_PATHS = ['images', 'documents']

TIMEZONE = 'Europe/Dublin'

DEFAULT_LANG = 'en'
DEFAULT_DATE_FORMAT = '%d/%m/%Y'
DATE_FORMATS = {
    'en': '%d/%m/%Y',
}

THEME = "themes/tanadaaron-theme"

DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False

MENU_ITEMS = (
    ('about', '#'),
    ('methodology', '#'),
)



# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True